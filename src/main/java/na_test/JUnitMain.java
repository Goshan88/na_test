package na_test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

class JUnitMain {
		String TEST_WORD_1 = "����";
		String TEST_WORD_2 = "����";
		String TEST_WORD_3 = "��������";
		
	@Test
	void checkIncrementWordCount() {
		List<Word> word_count = new ArrayList<Word>(Arrays.asList());
		word_count.add(new Word(TEST_WORD_1,0));
		Main.incrementCount(word_count,TEST_WORD_1);
		assertEquals(1, word_count.get(0).getCount());
	}
	
	@Test
	void checkSort() {
		List<Word> word_count = new ArrayList<Word>(Arrays.asList());
		word_count.add(new Word(TEST_WORD_1,0));
		word_count.add(new Word(TEST_WORD_2,1));
		word_count.add(new Word(TEST_WORD_3,2));
		Collections.sort(word_count, new WordSort());
		
		assertEquals(2, word_count.get(0).getCount());
	}

}
