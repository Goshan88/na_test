package na_test;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;


class Word {
    private String word;
    private int count;

    public Word(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String name) {
        this.word = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int age) {
        this.count = age;
    }
}

class WordSort implements Comparator<Word>{
	 
    @Override
    public int compare(Word e1, Word e2) {
        if(e1.getCount() < e2.getCount()){
            return 1;
        } else {
            return -1;
        }
    }
}

public class Main {
	
	public static void main(String[] args){
		int MINIMAL_WORD_LENGTH = 3;//������� ��� ����� ������ ���� ������ 3 ��������, �� ���� "���" �� ����� ��������� �� �����
		String COMMAND_FOR_QUIT = "quit";// ������� �� �����
		boolean error_load_file = false;
		
		ArrayList words = new ArrayList();
		List<Word> word_count = new ArrayList<Word>(Arrays.asList());
		Stream<String> lines = null;
		
		while(true) {
			
			System.out.println("Please enter directory for the file(like this: D:\\\\test.txt)");
			Scanner in = new Scanner(System.in); 
			String file_name = in.nextLine();
			if(file_name.equals(COMMAND_FOR_QUIT)) {
				System.out.println("Close application");
				System.exit(0);
			}
			
			
				
		try {
			lines = Files.lines(Paths.get(file_name));//D:\\test.txt
			error_load_file = false;
		}catch(IOException except){
			System.out.println("Error read file. Please check path or name file: " + file_name);
			error_load_file = true;
		}
		
			if(!error_load_file) {
				//������� ������� �� ��������, ����� ����� �������� ������
				lines.forEach(s -> {int begin=0,end=0;
									boolean first_word=false;
									for(int i=0;i<s.length();i++) {
									if((s.charAt(i) == ' ')||(s.charAt(i) == '.')||(s.charAt(i) == ',')) {
										if(i>MINIMAL_WORD_LENGTH) {
											if(!first_word) {
												if(i > MINIMAL_WORD_LENGTH) { 
													String tmp = s.substring(0,(i));
													words.add(tmp);
													first_word = true;
													begin = i+1;
												}
											}else {
												if((i-begin) > MINIMAL_WORD_LENGTH) { 
													String tmp = s.substring(begin,i);
													words.add(tmp);
													begin = i+1;
												}
											}
										}
									}
				}});
				lines.close();
				
				//������������� �������, ���� ���� ���������� ����� ��, ������� ��� � ������, ���� ��� ����� ����������� ��������, �� �������������� �������.
				for(int i=0; i<words.size(); i++) {
					boolean have_element=false;
					if(word_count.size()==0) {
						word_count.add(new Word(words.get(i).toString(),0));
					}else {
						for(int j=0; j<word_count.size(); j++) {
							if(word_count.get(j).getWord().equals(words.get(i).toString())) {
								incrementCount(word_count,word_count.get(j).getWord());
								have_element = true; //����� �������� ���� � ������
							}
						}
						if(!have_element) {//����������� ���� ������, ������ �������� �� �����, ������ ��������� ���
							word_count.add(new Word(words.get(i).toString(),0));
						}
					}
				}
				
				Collections.sort(word_count, new WordSort());
				
				//System.out.println(word_count.size());
				word_count.forEach(word-> {
					System.out.println("Word: " + word.getWord() + ", Count: " + (word.getCount()+1));
				});
		
			}//if error_load_file
		}//while
	}
	
	public static void incrementCount(List<Word> listOfWords, String name) {
		for (Word entity : listOfWords){
		       if(entity.getWord() == name) {
		    	   int tmp = entity.getCount();
		    	   entity.setCount(++tmp);
		       }
		   }
	}
	
}
